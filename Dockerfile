FROM python:3.8-alpine

RUN pip install pylint
RUN pip install pylint-exit

RUN pip install pylint-gitlab

ENV PYTHONUNBUFFERED True

WORKDIR /opt/
COPY . .
