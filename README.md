<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [pylint](#pylint)
- [Commands](#commands)
  - [Smoke Test](#smoke-test)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

[![pipeline status](https://gitlab.com/bswrundquist/code-quality/lint/pylint/badges/master/pipeline.svg)](https://gitlab.com/bswrundquist/code-quality/lint/pylint/-/commits/master)

# pylint

# Commands

In the *Makefile* there are `make` commands defined to simplify local development.

## Smoke Test

This will create an image and run the CLI `pylint`. 

Smoke tests goes beyond unit testing by making sure it can run within an already standing infrastructure.

A generic smoke test can be run as follows. 

Look in the Makefile to add commands to the smoke test as you change the CLI. 

```bash
make smoke-test
```